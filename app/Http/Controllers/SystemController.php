<?php

namespace App\Http\Controllers;

use App\System;
use Illuminate\Http\Request;
use App\Module;
use DataTables;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::all();
        return view('system.index', compact('modules'));
    }

    public function getSystems(Request $request)
    {

        $systems = Module::all();
        if ($request->ajax()) {
            $data = System::all();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) {
                           $btn = '<a href="/" class="edit btn btn-primary btn-sm">Edit</a><a href="/" class="edit btn btn-warning btn-sm">Delete</a>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('system.index', [
            "modules" => $systems,
        ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $module = new System();
        $module->name = $request->name;
        $module->desc = $request->desc;
        $module->ordering = $request->ordering;
        $module->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function show(System $system)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function edit(System $system)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, System $system)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function destroy(System $system)
    {
        //
    }
}
