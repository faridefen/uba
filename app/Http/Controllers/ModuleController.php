<?php

namespace App\Http\Controllers;

use App\Module;
use App\System;
use Illuminate\Http\Request;
use DataTables;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::all();
        $systems = System::all();
        return view('modules.index', [
            "modules" => $modules,
            "systems" => $systems
        ]);
    }

    public function getModules(Request $request)
    {

        $modules = Module::all();
        $systems = System::all();
        if ($request->ajax()) {
            $data = Module::all();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row) {
                           $btn = '<div style="display:block"><button onclick="edit_action(this,'.$row->id.')" type="button" class="btn btn-primary m-2" data-bs-toggle="modal" data-bs-target="#editmoduleModal">Edit</button><a href="/modules/delete/'.$row->id.'" type="button" class="btn btn-primary"><i class="fa fa-remove"></i>Delete</a href="/modules/delete/'.$row->id.'"></div>';
                           return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('modules.index', [
            "modules" => $modules,
            "systems" => $systems
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $module = new Module();
        $module->system = $request->system;
        $module->name = $request->name;
        $module->desc = $request->desc;
        $module->filename = $request->filename;
        $module->classname = $request->classname;
        $module->ordering = $request->ordering;
        $module->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        $module->system = $request->system;
        $module->name = $request->name;
        $module->desc = $request->desc;
        $module->filename = $request->filename;
        $module->classname = $request->classname;
        $module->ordering = $request->ordering;

        $module->update();

        return redirect()->route('module');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        $module->delete();

        return redirect()->route('module');
    }
}
