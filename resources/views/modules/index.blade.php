@extends('layouts.admin')
<script src="https://code.jquery.com/jquery-3.5.0.js" integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>This is modules management index page</h4></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @include('modules.create')
                    @include('modules.edit')
                    <input type="hidden" id="item_id" value="0" />
                    <hr>
                    <table id="modulesTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ordering</th>
                                <th>System Name</th>
                                <th>Module Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                        
                    </table>
                    <button action="edit_action(this)">click me</button>

                    <script type="text/javascript">
                    var YajraDataTable;
                    $(document).ready(function() {
                        YajraDataTable = $('#modulesTable').DataTable({
                            // processing: true,
                            serverSide: true,
                            ajax: "{{ url('modules')}}",
                            columns: [
                                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                                {data: 'ordering', name: 'ordering'},
                                {data: 'system', name: 'system'},
                                {data: 'name', name: 'name', orderable: true},
                                {data: 'action', name: 'action', orderable: false, searchable: false},
                            ]
                        });
                    });

                    var YajraDataTable =  $('#modulesTable');
                    // edit action
                    function edit_action(this_el, item_id){
                        console.log(this_el, item_id);
                        $('#item_id').val(item_id);
                        var tr_el = this_el.closest('tr');
                        var row = YajraDataTable.row(tr_el);
                        var row_data = row.data();
                        console.log(row_data);
                        var actions =  '/modules/edit/'+row_data.id;
                        $('#editModuleForm').attr('action', actions)
                        $('#moduleName').val(row_data.name);
                        $('#moduleSystem').val(row_data.system);
                        $('#moduleDesc').val(row_data.desc);
                        $('#moduleFileName').val(row_data.file);
                        $('#moduleClassName').val(row_data.class);
                        $('#moduleOrderingName').val(row_data.ordering);
                    }

                  </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
