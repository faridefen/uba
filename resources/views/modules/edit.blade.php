

<!-- Modal -->
<div class="modal modal-lg fade" id="editmoduleModal" tabindex="-1" aria-labelledby="editmoduleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="editmoduleModalLabel">Module</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="editModuleForm" class="form-floating" action="" method="POST">
          @csrf
          @method('PUT')
          <div class="row mb-3">
            <div class="col-md-6">
              <label >Module Name</label>
              <input class="form-control" type="text" name="name" placeholder="Module Name" id="moduleName">
            </div>
            <div class="col-md-6">
              <label >Select System Module</label>
              <select class="form-control" name="system" id="moduleSystem">
                @foreach($systems as $system)
                  <option class="form-control" value="{{ $system->id }}">{{ $system->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-12">
              <label >Description</label>
              <input class="form-control" type="textarea" name="desc" placeholder="Module Description" id="moduleDesc">
            </div>
            <div class="col-md-6">
              <label >File Name</label>
              <input class="form-control" type="text" name="filename" placeholder="File Name" id="moduleFileName">
            </div>
            <div class="col-md-6">
              <label >Class Name</label>
              <input class="form-control" type="text" name="classname" placeholder="Class Name" id="moduleClassName">
            </div>
            <div class="col-md-6">
              <label >Ordering</label>
              <input class="form-control" type="number" name="ordering" placeholder="Ordering Number" id="moduleOrderingName">
            </div>
          </div>
          <button class="btn btn-primary">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>