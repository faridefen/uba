<div class="modal modal-danger fade" id="modal_delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Language</h4>
            </div>
            <div class="modal-body">
                <p>Are You sure You want to delete this Language?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancel</button>
                <button id="delete_action" type="button" class="btn btn-outline">Submit</button>
            </div>
        </div>
    </div>
</div>