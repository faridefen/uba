                    <table id="modulesTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ordering</th>
                                <th>System Name</th>
                                <th>Module Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                        
                    </table>


                    <script type="text/javascript">
                    $(document).ready(function() {
                        var table = $('#modulesTable').DataTable({
                            // processing: true,
                            serverSide: true,
                            ajax: "{{ url('modules')}}",
                            columns: [
                                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                                {data: 'ordering', name: 'ordering'},
                                {data: 'system', name: 'system'},
                                {data: 'name', name: 'name', orderable: true},
                                {data: 'action', name: 'action', orderable: false, searchable: false},
                            ]
                        });
                    });
                  </script>