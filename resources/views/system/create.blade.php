<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#moduleModal">
  Add System
</button>

<!-- Modal -->
<div class="modal modal-lg fade" id="moduleModal" tabindex="-1" aria-labelledby="moduleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="moduleModalLabel">Systems</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="form-floating" action="{{ route('system.add') }}" method="POST">
          @csrf
          @method('POST')
          <div class="row mb-3">
            <div class="col-md-6">
              <label >Name</label>
              <input class="form-control" type="text" name="name" placeholder="Module Name">
            </div>
            <div class="col-md-12">
              <label >Description</label>
              <input class="form-control" type="textarea" name="desc" placeholder="Module Description">
            </div>
            <div class="col-md-6">
              <label >Ordering</label>
              <input class="form-control" type="number" name="ordering" placeholder="Ordering Number">
            </div>
          </div>
          <button class="btn btn-primary">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>