<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/modules', 'ModuleController@getModules')->name('module');
Route::post('/modules', 'ModuleController@store')->name('module.add');
Route::put('/modules/edit/{module}', 'ModuleController@update')->name('module.update');
Route::get('/modules/delete/{module}', 'ModuleController@destroy')->name('module.destroy');

Route::get('/systems', 'SystemController@getSystems')->name('system');
Route::post('/systems', 'SystemController@store')->name('system.add');

Route::get('/actions', 'ActionController@index')->name('action.index');
Route::get('/getactions', 'ActionController@getActions')->name('action');
Route::post('/actions', 'ActionController@store')->name('action.add');

Route::get("getModules", "UserController@getUsers");